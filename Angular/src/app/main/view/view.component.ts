import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';

import { HeatmapComponent } from "./heatmap/heatmap.component";
import { DataService } from "../data.service";

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
