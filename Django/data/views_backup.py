import json

from django.http import HttpResponse
from django.shortcuts import get_list_or_404, get_object_or_404, render
from matplotlib.patches import Rectangle
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

from .models import Image
import data.filters as filters
import data.highlights as highlights
from data.loader import DATA

matplotlib.use('Agg')


def get_all_fields_values(request):
    d = {species: {key: [str(v) for v in value] for key, value in DATA[species][
        "fields"].items()} for species in DATA.keys()}
    return HttpResponse(json.dumps(d), content_type='json')


def get_fields_values_species(request, species):
    return HttpResponse(json.dumps(DATA[species]["fields"]), content_type='json')

# Create your views here.


def index(request):
    avail = list(DATA.keys())
    context = {"avail": avail}
    return render(request, 'data/index.html', context)


def table(request, species, type):
    df = DATA[species][type]
    df = df.to_html()
    context = {"data": df, "species": species, "type": type}
    return render(request, 'data/table.html', context)


def image(request, species, size):
    #    image = Image.objects.filter(species=species)
    #    image = get_object_or_404(image, size=size)
    context = {"image": "%s/%s/clustermap.png" % (species, size), "species": species, "size": size}
    return render(request, 'data/img.html', context)


def filter_image(request, species, size):
    context = filters.filter_and_draw(DATA, request.GET.dict()["filter"], species, size)
    return context["response"]


def filter_and_highlight_image_angular(request):
    d = request.GET.dict()
    d = eval(d["q"])
    species = d["species"]
    size = d["size"]
    d['filters'] = {f['field']: [str(v) for v in f['values']] for f in d['filters']}
#    d['highlights'] = {f['field']: f['value'] for f in d['highlights'] }
    print(d)
    context = highlights.filter_and_highlight_complete(d, DATA, species, size)
    return context['response']


def filter_and_highlight_image(request, species, size):
    context = highlights.filter_and_highlight_complete(request, DATA, species, size)
    return context['response']


def filter_and_highlight_meta(request, species, size):
    context = highlights.filter_and_highlight_complete(request, data, species, size)
    return HttpResponse(context['meta'].to_json(orient='index'), content_type='json')
