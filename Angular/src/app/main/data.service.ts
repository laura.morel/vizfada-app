import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, Subject, ReplaySubject, BehaviorSubject} from 'rxjs';

import { CookieService } from "ngx-cookie-service";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private baseURL = 'http://127.0.0.1:8000';
  public submittedData = new Subject;

  constructor(private http: HttpClient, private cookieService: CookieService) {
  }

  get_legend(): Observable<Object> {
    let url = `${this.baseURL}/api/legend`;
    console.log("Fetching legend at ", url);
    return this.http.get(url, {responseType: 'json', withCredentials: true})
  }

  submit_data(data: Object): void {
    this.submittedData.next(data);
  }

  filter_highlight(data: Object): Observable<Blob> {
//    const sp = data['species'].replace(" ", "_");
    let url: string = `${this.baseURL}/api/img`;
    console.log(data);
    url = url + `?q=${JSON.stringify(data)}`;
    console.log("Fetching image at ", url);
//    return url;
    return this.http.get(`${url}`, {responseType: 'blob', reportProgress: true, withCredentials: true});
  }

  get_fields(data: Object): Observable<Object> {
    let url = `${this.baseURL}/api/fields`;
    url = url + `?q=${JSON.stringify(data)}`;
    console.log("Fetching metadata at ", url);
    return this.http.get(url, {responseType: 'json'})
  }

  get_species(): Observable<Object> {
    let url = `${this.baseURL}/api/species`;
    console.log("Fetching available species");
    return this.http.get(url, {responseType: 'json'})
  }
}
