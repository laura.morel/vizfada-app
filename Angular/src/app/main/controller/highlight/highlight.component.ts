import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { IDropdownSettings } from 'ng-multiselect-dropdown';

import { DataService } from '../../data.service';

@Component({
  selector: 'app-highlight',
  templateUrl: './highlight.component.html',
  styleUrls: ['./highlight.component.css']
})
export class HighlightComponent implements OnInit {
  @Input() formGroup: FormGroup;
  @Input() METADATA: Object;
  @Input() color: string;

  public fields: string[];
  public values: string[];
  private selectedValues: string[] = [];

  public fieldDropdownSettings: IDropdownSettings = {};
  public valuesDropdownSettings: IDropdownSettings = {};

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.fields = Object.keys(this.METADATA);
    console.log(this.fields)
    this.fieldDropdownSettings = {
      singleSelection: true,
      allowSearchFilter: true,
      closeDropDownOnSelection: true
    };
    this.valuesDropdownSettings = {
      singleSelection: false,
      allowSearchFilter: true,
    }
  }

  getValues(e): void {
    let selected = e;
    console.log("Selected ", selected)
    this.formGroup.controls['field'].setValue(selected);
    this.dataService.get_fields(this.formGroup.value)
                    .subscribe(meta => {this.METADATA = meta});
    this.values = this.METADATA[selected]
  }

  addSelected(e): void {
    this.selectedValues.push(e);
  }

  removeSelected(e): void {
    let i=this.selectedValues.indexOf(e);
    console.log(e, " found at ", i, " in ", this.selectedValues)
    this.selectedValues.splice(i, 1);
    console.log(this.selectedValues)
  }

  onSelectValue(e): void {
    let selected = this.addSelected(e);
    console.log("Selected value ", e);
    this.formGroup.controls['values'].setValue(this.selectedValues);
    console.log("Currently selected ", this.formGroup.controls['values'])
  }

  onChangeColor(e): void {
    this.color = e;
    this.formGroup.controls['color'].setValue(e.substr(1));
    console.log("Changing color to ", e);
  }

  onDeSelectValue(e): void {
    let selected = this.removeSelected(e);
    console.log("Deselected value ", e);
    this.formGroup.controls['values'].setValue(this.selectedValues);
    console.log("Currently selected ", this.formGroup.controls['values'])
  }
}
