import { Injectable } from '@angular/core';
import { ReplaySubject, BehaviorSubject} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class HeatmapInfoService {

  public pxcolor = new ReplaySubject;
  public imgLoaded = new BehaviorSubject(false);

  constructor() { }

  pixel_color(color: string): void {
    this.pxcolor.next(color);
  }

  img_loaded(bool: boolean): void {
    this.imgLoaded.next(bool);
  }

}
