from django.core.management.base import BaseCommand, CommandError
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pickle
import seaborn as sns
from data.models import Correlation, Image
import os


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'
    species = ["Bos_taurus", "Capra_hircus", "Equus_caballus",
               "Gallus_gallus", "Ovis_aries", "Sus_scrofa"]
    sizes = [10, 15, 20]

    def add_arguments(self, parser):
        parser.add_argument('verbose', type=bool, nargs='?', default=False)

    def fill_db(self, verbose):
        for sp in self.species:
            if verbose:
                print(sp)
            spDir = "./assets/" + sp
            metaPath = spDir + "/metadata.tsv"
            corPath = spDir + "/correlation.csv"
            with open(metaPath) as meta:
                metadata = pd.read_csv(meta, sep="\t", header=0, index_col="run_accession")
            with open(corPath) as correlation:
                correlation = pd.read_csv(correlation, sep=",", header=0, index_col=0)
            c = Correlation(species=sp, correlation=spDir +
                            "/corPickle", metadata=spDir + "/metaPickle")
            metadata = metadata.applymap(str)
            c.pickle(correlation, "correlation")
            c.pickle(metadata, "metadata")
            c.save()
            if verbose:
                print("Saved %s." % (sp))

            for s in self.sizes:
                if verbose:
                    print(str(s))
                try:
                    os.mkdir("%s/%d" % (spDir, s))
                except FileExistsError:
                    pass
                fig = sns.clustermap(correlation, figsize=(s, s))
                png = "%s/%d/clustermap.png" % (spDir, s)
                fig.savefig(png, transparent=False)
                i = Image(species=c, plot="%s/%s/figPickle" % (spDir, s),
                          size=s, png="%s/%d/clustermap.png" % (spDir, s))
                i.pickle(fig)
                i.save()

    def handle(self, **options):
        self.fill_db(options['verbose'])
