import { Component, ViewChild, OnInit, Input} from '@angular/core';
import { Validators, FormGroup, FormArray, FormBuilder, FormControl } from '@angular/forms';

import { NgxSpinnerService } from "ngx-spinner";

import { LegendComponent } from '../view/heatmap/legend/legend.component';
import { FilterComponent } from './filter/filter.component';
import { DataService } from '../data.service';
import { HeatmapInfoService } from '../heatmap-info.service';

@Component({
  selector: 'app-controller',
  templateUrl: './controller.component.html',
  styleUrls: ['./controller.component.css']
})
export class ControllerComponent implements OnInit {

  @ViewChild(LegendComponent) legend: LegendComponent;

  public SPECIES = ["Gallus_gallus", "Bos_taurus", "Ovis_aries", "Capra_hircus", "Equus_caballus", "Sus_scrofa"];
  public SIZES = ["10", "15", "20"];
  public METADATA: Object;

  public fields: string[];

  public formGroup: FormGroup;

  constructor(private builder: FormBuilder,
              private dataService: DataService,
              private spinner: NgxSpinnerService,
              private heatmapInfoService: HeatmapInfoService,) { }

  ngOnInit(): void {
    let newForm = this.builder.group({
      species: ["", [Validators.required]],
      filters: this.builder.array([]),
      highlights: this.builder.array([]),
      annotated: "",
      options: this.builder.group({})
    });
    console.log(newForm);
    this.formGroup = newForm;

  }

  addFilter():void {
    console.log("Adding filter")
    const arrayControl = <FormArray>this.formGroup.controls['filters'];
    let newGroup = this.builder.group({
      field: ['', [Validators.required]],
      values: [[], [Validators.required]]
    });
    arrayControl.push(newGroup);
    this.getMetadata()
  }

  addHighlight(): void {
    console.log("Adding highlight")
    const arrayControl = <FormArray>this.formGroup.controls['highlights'];
    let newGroup = this.builder.group({
      field: ['', [Validators.required]],
      values: [[], [Validators.required]],
      color: ["0082ff", [Validators.required]]
    });
    arrayControl.push(newGroup);
    this.getMetadata()
  }

  getMetadata(): void {
    console.log(this.formGroup.value);
    this.dataService.get_fields(this.formGroup.value)
                    .subscribe(meta => {this.METADATA = meta; this.fields = Object.keys(meta)});
  }

  get formFilters() {return <FormArray>this.formGroup.get('filters')}

  get formHighlights() {return <FormArray>this.formGroup.get('highlights')}

  delFromArray(name: string, index: number): void {
    console.log("Removing filter")
    const arrayControl = <FormArray>this.formGroup.controls[name];
    arrayControl.removeAt(index);
    this.getMetadata()
  }

  changeSpecies(e) {
    console.log("Changing species")
    this.formGroup.controls['species'].setValue(e.target.value);
    this.getMetadata()
  }

  changeSize(e) {
    console.log("Changing size")
    const s = e.target.value;
    let optionsControl = <FormGroup>this.formGroup.controls['options'];
    try {
      optionsControl.controls['figsize'].setValue(`(${s}, ${s})`);
    } catch (error) {
      optionsControl.addControl('figsize', new FormControl(`(${s}, ${s})`));
    }
    this.formGroup.controls['options'] = optionsControl;
  }

  changeAnnotation(e) {
    console.log("Changing annotated field");
    this.formGroup.controls['annotated'].setValue(e.target.value);
  }

  onSubmit(): void {
    console.log("Submitting data")
    console.log(this.formGroup.value);
    this.dataService.submit_data(this.formGroup.value);
    this.heatmapInfoService.img_loaded(false);
  }

}
