from data.models import Correlation
#from .models import Correlation
import seaborn as sns
import pandas as pd


def load_data():
    """ Load correlations and metadata from database into a dict object"""
    data = {}
    for entry in Correlation.objects.all():
        meta = entry.unpickle("metadata")
        correlation = entry.unpickle("correlation")
        agg = {k: list(pd.unique(v)) for k, v in meta.to_dict(orient="list").items()}
        data[entry.species] = {"metadata": meta, "correlation": correlation, "fields": agg}
    return data

DATA = load_data()
