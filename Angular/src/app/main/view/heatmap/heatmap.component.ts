import { Component, OnInit, Input, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
import { CookieService } from "ngx-cookie-service";

import { DataService } from '../../data.service';
import { HeatmapInfoService } from '../../heatmap-info.service';
import { LegendComponent} from './legend/legend.component';
import { CanvasComponent } from './canvas/canvas.component';

@Component({
  selector: 'app-heatmap',
  templateUrl: './heatmap.component.html',
  styleUrls: ['./heatmap.component.css']
})
export class HeatmapComponent implements OnInit {

  @ViewChild(CanvasComponent) canvas: CanvasComponent;

  public formdata: Object;

  public src: string = "no_heatmap";
  public img: HTMLImageElement;
  public imgLoading: boolean = false;

  constructor(private dataService: DataService,
              private heatmapInfoService: HeatmapInfoService,
              private sanitizer: DomSanitizer,
              private spinner: NgxSpinnerService,
              private cookieService: CookieService,
            ) { }

  ngOnInit(): void {
    this.spinner.show();
    this.dataService.submittedData.subscribe(data => {
      this.formdata = data;
      this.setImg();
    });
  }

  setImg(): void{
    this.imgLoading = true;
    this.dataService.filter_highlight(this.formdata).subscribe(img => {
      this.imgFromBlob(img);
    }, error => {
      this.imgLoading = false;
      this.heatmapInfoService.img_loaded(false);
      console.log(error);
    });
  }


  setSrc(src: string): void{
    this.src = src;
    //let img = new Image();
    //img.src = this.src;
    //this.img = img;
  }

  imgFromBlob(img: Blob) {
    console.log("Getting URL from Blob");
    let reader = new FileReader();
    reader.addEventListener("load", () => {
      this.setSrc(reader.result as string);
      this.canvas.setImg(this.src);
      this.imgLoading = false;
      this.heatmapInfoService.img_loaded(true);
      console.log("Heatmap src changed to: ", this.src);
    }, false);

    if (img) {
      reader.readAsDataURL(img);
    }
  }


}
