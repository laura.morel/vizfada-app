from django.db import models
import pickle


class Correlation(models.Model):
    species = models.CharField(max_length=50, primary_key=True)
    correlation = models.CharField(max_length=200)
    metadata = models.CharField(max_length=200)

    def __str__(self):
        return "%s" % (self.species)

    class Meta:
        indexes = [
            models.Index(fields=["species"]),
        ]

    def unpickle(self, attr):
        return pickle.load(open(getattr(self, attr), 'rb'))

    def pickle(self, obj, attr):
        return pickle.dump(obj, open(getattr(self, attr), 'wb'))


class Image(models.Model):
    species = models.ForeignKey(Correlation, on_delete=models.CASCADE)
    plot = models.CharField(max_length=200)
    size = models.PositiveSmallIntegerField()
    png = models.CharField(max_length=200)

    def __str__(self):
        return "%s %s" % (self.species.species, self.size)

    def unpickle(self):
        return pickle.load(open(self.plot, 'rb'))

    def pickle(self, obj):
        return pickle.dump(obj, open(self.plot, 'wb'))
