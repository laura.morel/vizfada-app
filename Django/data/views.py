import json

from django.http import HttpResponse
from django.shortcuts import get_list_or_404, get_object_or_404, render
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from data.heatmap.heatmap import ClusteredHeatmap
from data.models import Correlation

CURRENT_HEATMAP = None


def index(request):
    pass


def query_handler(request):
    q = request.GET.dict()['q']
    params = eval(q)
    # TODO: Change when seaborn options implemented in front-end
    filters = {}
    highlights = {}
    for dict in params['filters']:
        if dict['field'] != '':
            filters.setdefault(dict["field"], []).extend(dict['values'])
    for dict in params['highlights']:
        if dict['field'] != '':
            color = "#%s" % dict['color']
            highlights.setdefault(color, {}).update({dict["field"]: dict["values"]})
    options = {opt: eval(val) for opt, val in params["options"].items()}
    print("Options: %s" % (str(options)))
    heatmap = ClusteredHeatmap(params["species"], options)
    heatmap.add_filters(filters)
    if params["annotated"] != "":
        heatmap.annotate_field(params["annotated"])
    heatmap.add_highlights(highlights)
    request.session['legend'] = heatmap.annotation_legend
    print(heatmap)
    return heatmap


def get_image(request):
    heatmap = query_handler(request)
    print("Kept %d/%d" % (heatmap.metadata.shape[0], heatmap.ALL_METADATA.shape[0]))
    print(heatmap.highlights)
    fig = heatmap.get_heatmap(highlighted=True)
    # print(heatmap)
    response = HttpResponse(content_type="image/png")
    plt.savefig(response)
    request.session['legend'] = heatmap.annotation_legend
    request.session.save()
    print("Session key (image): \n%s" % (str(request.session.session_key)))
    print("Session legend: \n%s" % (str(request.session['legend'])))
    request.session.set_test_cookie()
    return response


def get_legend(request):
    # try:
    #    sessionID = request.COOKIES.get('sessionID')
    # except KeyError:
    #    return HttpResponse()
    #heatmap = query_handler(request)
    #legend = json.dumps(heatmap.annotation_legend)
    print("Test cookie worked: %s" %(str(request.session.test_cookie_worked())))
    print("Session key (legend): \n%s" % (str(request.session.session_key)))
    print("Session legend: \n%s" % (str(request.session['legend'])))
    legend = json.dumps(request.session['legend'])
    response = HttpResponse(legend, content_type="json")
    return response


def get_fields(request):
    heatmap = query_handler(request)
    fields = json.dumps(heatmap.get_fields())
    return HttpResponse(fields, content_type="json")


def get_species(request):
    species = Correlation.objects.values('species')
    species = [v for dct in species for k, v in dct.items()]
    return HttpResponse(json.dumps(species), content_type="json")
