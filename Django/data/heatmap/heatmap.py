import json
import typing

from django.http import HttpResponse
from django.shortcuts import get_list_or_404, get_object_or_404, render
from matplotlib.patches import Rectangle

import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from distinctipy import distinctipy
import scipy as scp

from data.models import Correlation

Filters = typing.Mapping[str, typing.List]
Highlights = typing.Mapping[str, Filters]
"""
  Filters type example:
    {"cellType": ["blood", "brain"], "sex": ["female"]}

  Highlights type example:
    {"blue": {"project_name": ["GENESWITCH"]},
     "red": {
            "organism_id": ["id1", "id2"],
            "article_published": ["yes"]
            }
    }
"""

DEFAULT_SNS_OPTIONS = {"cmap": "rocket_r"}


class ClusteredHeatmap:

    def __init__(self, species: str, seaborn_options: dict) -> None:
        print("Creating new ClusteredHeatmap")
        self.species = species
        self._load_data()
        self.correlation = self.ALL_CORRELATION
        self.metadata = self.ALL_METADATA
        self.filters = {}
        self.highlights = {}
        self.seaborn_options = DEFAULT_SNS_OPTIONS.copy()
        self.seaborn_options.update(seaborn_options)
        self.fig = None
        self.highlighted = None
        self.annotated = ""
        self.annotation_legend = {}
        self.get_fields()

    def __str__(self) -> str:
        return "ClusteredHeatmap: \n\t%s\n\t%s\n\t%s\n\t%s\n\t%s" % (self.species, str(self.filters), str(self.highlights), self.annotated, str(self.seaborn_options))

    def _filter_meta(self, filters: Filters) -> pd.DataFrame:
        print("Filtering metadata with filters %s" % (str(filters)))
        m = self.metadata
        for f, v in filters.items():
            try:
                m = m[m[f].isin(v)]
            except KeyError:
                return pd.DataFrame()
        print("Keeping %d samples" % (m.shape[0]))
        return m

    def _filter_corr(self) -> None:
        print("Filtering correlations...")
        selected = self.correlation.index.isin(self.metadata.index)
        self.correlation = self.correlation.loc[selected, selected]
        print("Keeping %s samples" % (self.correlation.shape[0]))

    def _reset_view(self) -> None:
        print("Resetting metadata and correlations...")
        self.metadata = self.ALL_METADATA
        self.correlation = self.ALL_CORRELATION

    def _update_view(self) -> None:
        print("Updating metadata and correlations...")
        self.metadata = self._filter_meta(self.filters)
        self._filter_corr()
#        self._draw_heatmap()

    def _load_data(self) -> None:
        print("Loading data from database...")
        q = Correlation.objects.get(species=self.species)
        self.ALL_METADATA = q.unpickle("metadata")
        self.ALL_CORRELATION = q.unpickle("correlation")

    def _highlight_fig(self) -> None:
        print("Highlighting figure...")
        if not self.fig:
            self._draw_heatmap()
        fig = self.fig
        ax = fig.ax_heatmap
        n = self.correlation.shape[0]
        for color, runs in self.highlights.items():
            for run in runs:
                i = self.correlation.columns.tolist().index(run)
                i = fig.dendrogram_col.reordered_ind.index(i)
                ax.add_patch(Rectangle((0, i), n, 1, fill=True, color=color, alpha=0.3, lw=0))
                ax.add_patch(Rectangle((i, 0), 1, n, fill=True, color=color, alpha=0.3, lw=0))
            for xtick, ytick in zip(ax.get_xticklabels(), ax.get_yticklabels()):
                if xtick.get_text() in runs:
                    xtick.set_color(color)
                if ytick.get_text() in runs:
                    ytick.set_color(color)
        self.highlighted = fig

    def _draw_heatmap(self) -> None:
        print("Computing linkage and drawing heatmap...")
        dist = scp.spatial.distance.pdist(self.correlation)
        link = scp.cluster.hierarchy.linkage(dist, method="average")
        self.fig = sns.clustermap(self.correlation, row_linkage=link,
                                  col_linkage=link, **self.seaborn_options)

        """
        if self.annotated:
            for label, color in self.annotation_legend.items():
                self.fig.ax_col_dendrogram.bar(0, 0, color=color, label=label, linewidth=0)
            self.fig.ax_col_dendrogram.legend(loc="center", ncol=4)
        """

        self.highlighted = self.fig

    def _annotate(self) -> None:
        print("Annotating heatmap..")
        field = self.annotated
        try:
            values = self.fields[field]
        except KeyError:
            return None

        match = dict(zip(values, distinctipy.get_colors(len(values))))
        print(self.fields[field])
        print(match)
        print(self.metadata[field])
        row_colors = self.metadata[field].map(match)
        self.seaborn_options.update({"row_colors": row_colors})
        self.annotation_legend = match

    def annotate_field(self, field: str) -> None:
        self.get_fields()
        self.annotated = field
        self._annotate()

    def add_filters(self, filters: Filters) -> None:
        if not filters == {}:
            print("Adding filters...")
            for f, v in filters.items():
                print("%s %s" % (f, str(v)))
                self.filters.setdefault(f, [])
                self.filters.update({f: list(set(self.filters[f]).union(set(v)))})
            self._reset_view()
            self._update_view()

    def remove_filters(self, filters: Filters) -> None:
        print("Removing filters...")
        for f, v in filters.items():
            self.filters.setdefault(f, [])
            self.filters.update({f: list(set(self.filters[f]) - set(v))})
            if self.filters[f] == []:
                self.filters.pop(f)
        self._reset_view()
        self._update_view()

    def add_highlights(self, highlights: Highlights) -> None:
        if not highlights == {}:
            print("Adding highlights...")
            for color, filters in highlights.items():
                self.highlights.setdefault(color, {})
                runs = self._filter_meta(filters).index
                self.highlights.update({color: list(set(self.highlights[color]).union(set(runs)))})
            self._highlight_fig()

    def remove_highlights(self, highlights: Highlights) -> None:
        print("Removing highlights...")
        for color, runs in highlights.items():
            self.highlights.setdefault(color, [])
            self.highlights.update({color: list(set(self.highlights[color]) - set(runs))})
        self._highlight_fig()

    def get_fields(self) -> pd.DataFrame:
        self.fields = {f: [str(v) for v in pd.unique(vals)]
                       for f, vals in self.metadata.to_dict(orient="list").items()}
        return self.fields

    def get_heatmap(self, highlighted: bool=True) -> sns.matrix.ClusterGrid:
        if not highlighted:
            if not self.fig:
                self._draw_heatmap()
            return self.fig
        else:
            if not self.highlighted:
                self._highlight_fig()
            return self.highlighted
