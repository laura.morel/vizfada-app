import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ColorPickerModule } from 'ngx-color-picker';
import { NgxSpinnerModule } from "ngx-spinner";
import { CookieService } from "ngx-cookie-service";

import { AppComponent } from './app.component';
import { ControllerComponent } from './main/controller/controller.component';
import { HeatmapComponent } from './main/view/heatmap/heatmap.component';
import { FilterComponent } from './main/controller/filter/filter.component';
import { HighlightComponent } from './main/controller/highlight/highlight.component';
import { LegendComponent } from './main/view/heatmap/legend/legend.component';
import { ViewComponent } from './main/view/view.component';
import { MainComponent } from './main/main.component';
import { CanvasComponent } from './main/view/heatmap/canvas/canvas.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';

import { faCaretDown, faTimes } from '@fortawesome/free-solid-svg-icons';

@NgModule({
  declarations: [
    AppComponent,
    ControllerComponent,
    HeatmapComponent,
    FilterComponent,
    HighlightComponent,
    LegendComponent,
    ViewComponent,
    MainComponent,
    CanvasComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgMultiSelectDropDownModule,
    ColorPickerModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    NgbModule,
    FlexLayoutModule,
    FontAwesomeModule,
  ],
  providers: [CookieService],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faCaretDown, faTimes);
  }
}
